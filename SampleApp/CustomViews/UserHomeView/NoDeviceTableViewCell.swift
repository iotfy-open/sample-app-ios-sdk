//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//
import UIKit

class NoDeviceTableViewCell: UITableViewCell {
    
    // MARK: - Private
    @IBOutlet private weak var mainBGView: UIView!
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.mainBGView.layer.cornerRadius = 8
        self.mainBGView.layer.masksToBounds = false
        self.mainBGView.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.mainBGView.layer.shadowColor = UIColor.lightGray.cgColor
        self.mainBGView.layer.shadowOpacity = 0.7
        self.mainBGView.layer.shadowRadius = 4
        self.prepareForReuse()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

// MARK: - NibInitializable
extension NoDeviceTableViewCell: NibInitializable {}

// MARK: - Reusable
extension NoDeviceTableViewCell: Reusable {}
