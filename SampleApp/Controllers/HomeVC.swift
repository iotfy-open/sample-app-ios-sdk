//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//


import UIKit
import MagicSDK

// MARK: - Private
private enum TableSection: Int, CaseIterable {
    case devices
}

class HomeVC: UIViewController {
    
    @IBOutlet private weak var deviceTableView: UITableView!
    @IBOutlet private weak var topHeader: UIImageView!
    @IBOutlet private weak var tintView: UIView!
    var userThings = [IoTfyDevice]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchUserDevices()
    }
    
    @IBAction func tapOnAddDevice(_ sender: UIButton) {
        let selectDeviceTypeVC = SelectDeviceTypeVC.instantiate()
        self.navigationController?.pushViewController(selectDeviceTypeVC, animated: true)
    }
    
    
    @IBAction func tapOnLogout(_ sender: UIButton) {
        MagicSDK.SDK.shared.getUserService()?.logoutUser(logoutRequestCb: self)
    }
    
    private func moveToLoginOptions() {
        appDelegate.moveToLoginScreen()
    }
    
    private func configureDevicesCell(cell: DevicesTableViewCell, indexPath: IndexPath) {
        cell.configure(devices: self.userThings)
        
        // OnTapDevice to open it in respective device's dashboard
        cell.didTappedOnDevice = { [weak self] device -> Void in
            guard let strongSelf = self else { return }
            switch device.type {
            case .typePlug:
                    let plugDashboardVC = PlugDashboardVC.instantiate(selectedDevice: device)
                    strongSelf.navigationController?.pushViewController(plugDashboardVC, animated: true)
                default:
                    print("NO DEVICE")
            }
        }
    }
    
    private func fetchUserDevices() {
        MagicSDK.SDK.shared.getUserService()?.getUserThings(getUserThingsCb: self)
    }
    
    
    private func setupTableView() {
        self.deviceTableView.backgroundColor = .clear
        self.deviceTableView.preventEmptyCellSeparators()
        self.deviceTableView.estimatedRowHeight = 100
        DevicesTableViewCell.registerCellNib(with: self.deviceTableView)
        NoDeviceTableViewCell.registerCellNib(with: self.deviceTableView)
    }
}

// MARK: - UITableViewDataSource
extension HomeVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section: TableSection = TableSection(rawValue: indexPath.section)!
        switch section {
            case .devices:
                if self.userThings.count > 0 {
                    
                    for thing in self.userThings {
                        MagicSDK.SDK.shared.getThingService()?.registerStateChangeListener(udid: thing.udid, thingChangeStateCb: self)
                    }
                    
                    let cell = DevicesTableViewCell.dequeueCell(from: tableView, atIndexPath: indexPath)
                    self.configureDevicesCell(cell: cell, indexPath: indexPath)
                    return cell
                } else {
                    let cell = NoDeviceTableViewCell.dequeueCell(from: tableView, atIndexPath: indexPath)
                    return cell
                }
        }
    }
}

// MARK: - UITableViewDelegate
extension HomeVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let section: TableSection = TableSection(rawValue: indexPath.section)!
        switch section {
            case .devices:
                var count = Double(self.userThings.count/2) + Double(self.userThings.count%2)
                if (count == 0) {
                    count = 1
                }
                let height = count * 120.0 + count + 90.0
                return CGFloat(height)
        }
    }
}
extension HomeVC: GetUserThingsCb {
    func onSuccess(things: [IoTfyDevice]) {
        
        for thing in things {
            if (thing.type == .typePlug || thing.type == .typeSwitchSet || thing.type == .typeLED) {
                self.userThings.append(thing)
            }
        }
        
        DispatchQueue.main.async {
            self.deviceTableView.reloadData()
        }
    }
    
    func onError(code: String, error: String) {
        print(error)
    }
}

extension HomeVC: LogoutRequestCb {
    func onSuccess() {
        DispatchQueue.main.async {
            appDelegate.moveToLoginScreen()
            print("user Logged out")
        }
    }
}

extension HomeVC: ThingStateChangeCb {
    func onStateReported(udid: String, state: StateReport) {
        print("State = \(state)")
    }
    
    func onDeviceConnectionStateChanged(udid: String, isOnline: Bool) {
        print("Connected = \(isOnline)")
    }
}
