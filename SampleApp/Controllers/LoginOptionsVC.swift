//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit
import MagicSDK

class LoginOptionsVC: UIViewController {
    
    @IBOutlet weak var versionNumber: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (MagicSDK.SDK.shared.getUserService()?.isUserLoggedIn() ?? false) {
            appDelegate.moveToHomeScreen()
        }
        
        let versionText = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let buildText = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        if (versionText != nil && buildText != nil) {
            self.versionNumber.text = "\(versionText ?? "") (\(buildText ?? ""))"
        }
    }
    
    @IBAction func tapOnSignIn(_ sender: UIButton) {
        self.doLoginOrSignUp()
    }
    
    @IBAction func tapOnSignUp(_ sender: UIButton) {
        self.doLoginOrSignUp()
    }
    
    private func doLoginOrSignUp() {
        MagicSDK.SDK.shared.getUserService()?.loginOrRegisterSharedUser(authId: "1075000105", loginRequestCb: self)
    }
    
    private func moveOnDashboardScreen() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            appDelegate.moveToLoaderScreen()
        }
    }
    
}
extension LoginOptionsVC: LoginRequestCb{
    func onSuccess() {
        self.moveOnDashboardScreen()
    }
    
    func onError(code: String, error: String) {
        print("errorCode \(code)")
    }
}
