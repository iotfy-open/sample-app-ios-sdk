//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//


import UIKit
import CoreLocation

class SelectDeviceTypeVC: UIViewController {
    
    @IBOutlet weak var connectionMode: UILabel!
    
    @IBOutlet weak var deviceTypeTableView: UITableView!
    @IBOutlet weak var deviceTypeBgView: UIView!
    
    var locationManager = CLLocationManager()
    
    let deviceNameValues = [("Plug","6_black_icon",DeviceType.deviceTypePlug.rawValue)]
    
    
    class func instantiate() -> SelectDeviceTypeVC {
        let selectDeviceTypeVC = AppStoryboard.Device.viewController(viewControllerClass: SelectDeviceTypeVC.self)
        return selectDeviceTypeVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.deviceTypeTableView.register(UINib(nibName: "AddDeviceTableViewCell", bundle: nil), forCellReuseIdentifier: "AddDeviceTableViewCell")
        self.deviceTypeTableView.rowHeight = UITableView.automaticDimension
        self.isLocationAccessEnabled()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.deviceTypeBgView.dropShadow(color: .lightGray, opacity: 0.71, offSet: CGSize(width: -1, height: -1), radius: 3, scale: true)
    }
    
    @IBAction func tapOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SelectDeviceTypeVC:UITableViewDataSource{
    // MARK: - UITable view data source method
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceNameValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.deviceTypeTableView.dequeueReusableCell(withIdentifier: "AddDeviceTableViewCell") as! AddDeviceTableViewCell
        
        cell.tag = indexPath.section
        cell.deviceTitleLabel.text = deviceNameValues[indexPath.row].0
        cell.deviceImageView.image = UIImage.init(named: deviceNameValues[indexPath.row].1)
        cell.deviceImageView.tintColor = UIColor.lightGray
        cell.deviceTitleLabel.textColor = UIColor.darkGray.withAlphaComponent(0.8)
        
        return cell
    }
    
}

extension SelectDeviceTypeVC:UITableViewDelegate{
    
    // MARK: - UITable view delegate methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    print("No access")
                case .authorizedAlways, .authorizedWhenInUse:
                    let connectDeviceVC = ConnectDeviceVC.instantiate(selectedDevice: nil, isFromQuickConnect: true)
                    self.navigationController?.pushViewController(connectDeviceVC, animated: true)
                @unknown default:
                    break
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func isLocationAccessEnabled() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .restricted, .denied:
                    let alert = UIAlertController(title: "Location Permission is needed", message: "To allow app to scan and connect with your nearby smart devices, please provide location permission to the app", preferredStyle: .alert)
                    let denyAction = UIAlertAction(title: "Deny", style: .default, handler: nil)
                    let allowAction = UIAlertAction.init(title: "Allow", style: .default) { (_) in
                        if let url = URL(string:UIApplication.openSettingsURLString) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                    
                    alert.addAction(denyAction)
                    alert.addAction(allowAction)
                    present(alert, animated: true, completion: nil)
                case .notDetermined:
                    locationManager.requestWhenInUseAuthorization()
                case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                default:
                    break
            }
        }
        
        else {
            let alert = UIAlertController(title: "Location Services are disabled", message: "App requires access to location to scan and connect with nearby smart devices. Please turn on Location Services from your Phone Settings > Privacy > Location Services", preferredStyle: .alert)
            let openSettingsAction = UIAlertAction.init(title: "Open Settings", style: .default) { (_) in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            }
            
            alert.addAction(openSettingsAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
}


extension UIView{
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
    
}
