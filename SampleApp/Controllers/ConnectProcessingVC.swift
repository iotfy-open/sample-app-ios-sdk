//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit
import NetworkExtension
import Lottie
import Socket
import Reachability
import NSData_FastHex
import Foundation
import MagicSDK


struct ScanDevice {
    var macAddress:String?
    var ipAddress:String?
    
    init(macAddress:String? = nil, ipAddress:String? = nil) {
        self.macAddress = macAddress
        self.ipAddress = ipAddress
    }
}

protocol ConnectProcessingDelegate {
    func didFinishTaskStats()
    func didFailedTaskStatus()
}

class ConnectProcessingVC: UIViewController {
    
    var localNetworkReachability = try! Reachability()
    
    @IBOutlet weak var bgCardView: UIView!
    @IBOutlet weak var scanningDevCounLbl: UILabel!
    
    @IBOutlet weak var configureDevice: UILabel!
    @IBOutlet weak var connectTypeTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var counter = 45
    @IBOutlet weak var configurentionSentBtn: UIButton!
    @IBOutlet weak var connectedDeviceBtn: UIButton!
    @IBOutlet weak var deviceConfiguredBtn: UIButton!
    @IBOutlet weak var connectTypeIcon: UIImageView!
    @IBOutlet weak var animationView: AnimationView!
    
    var sessionCounter = 0
    var delegate:ConnectProcessingDelegate?
    var userSSIDName: String = ""
    var userWiFiPassword: String = ""
    var ssidValue: String = ""
    var deviceUDID: String = ""
    var devicePassword: String = ""
    var isFromDeviceConnection = false
    var isFromQuickConnect = false
    
    var resultExpected = 0
    var alertController = UIAlertController()
    var messageResult = ""
    var resultCount = 0
    var okAction:UIAlertAction?
    var bssid: String?
    var scanDevices = [ScanDevice]()
    var APItimer = Timer()
    var counterValue = 0
    var userThings = [IoTfyDevice]()

    class func instantiate() -> ConnectProcessingVC {
        let controller = AppStoryboard.Device.viewController(viewControllerClass: ConnectProcessingVC.self)
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
            if self.counter > 0 {
                self.scanningDevCounLbl.text = String(self.counter)
                self.counter -= 1
            } else {
                Timer.invalidate()
            }
        }
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.animationSpeed = 0.5
        animationView.play()
        if isFromQuickConnect {
            self.configureDevice.text = "Scanning Devices"
            self.connectTypeIcon.image = UIImage.init(named: "global_connect_gray_icon")
            self.connectTypeTitle.text = "Quick Connect"
        }
        self.connectFromWiFi(userSSIDName, devicePass: userWiFiPassword)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFromQuickConnect{
            self.connectTypeTitle.text = "Quick Connect"
            self.connectTypeIcon.image = UIImage.init(named: "global_connect_gray_icon")
        }
    }
    
    
    
    func connectFromWiFi(_ deviceSSID: String, devicePass:String) {
        MagicSDK.SDK.shared.getThingService()?.onboardViaQuickConnect(wifiSSID: deviceSSID, wifiPassword: devicePass, quickConnectTaskCb: self)
    }
    
}
extension ConnectProcessingVC: QuickConnectTaskCb  {
    func onError(errorCode: QuickConnectTaskErrCode) {
        print("errorCode \(errorCode)")
    }
    
    func onDeviceFound(udid: String) {
        self.resultCount = self.resultCount + 1
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.configureDevice.text = "\(strongSelf.resultCount) device (s) found. Please wait ..."
            strongSelf.configurentionSentBtn.setImage(#imageLiteral(resourceName: "checked_box_icon"), for: .normal)
        }
    }
    
    func onCompleted(things: [IoTfyDevice]) {
        self.userThings = things
        DispatchQueue.main.async {
            appDelegate.moveToHomeScreen()
        }
    }
    
}

