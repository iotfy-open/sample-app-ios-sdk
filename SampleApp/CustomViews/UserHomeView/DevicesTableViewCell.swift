//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit
import MagicSDK


class DevicesTableViewCell: UITableViewCell {
    
    // MARK: - Public
    
    func configure(devices: [IoTfyDevice]) {
        self.myDevices = devices
    }
    
    var didTappedOnOff: ((_ device: IoTfyDevice) -> Void)?
    var didTappedOnQuickMenu: ((_ device: IoTfyDevice) -> Void)?
    var didTappedOnDevice: ((_ cell: IoTfyDevice) -> Void)?
    var didTappedOnGroupFilter: (() -> Void)?
    
    // MARK: - Private
    @IBOutlet private weak var groupButton: UIButton!
    @IBOutlet private weak var mainBGView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var yellowImageView: UIImageView!
    @IBOutlet weak var deviceTypeListView: UICollectionView!
    
    private let itemsPerRow: CGFloat = 2
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 10.0, right: 10.0)
    private var myDevices: [IoTfyDevice] = [IoTfyDevice]()
    var cellDict = [String:Int]()
    var groupCellDict = [String: Int]()
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DeviceTypeCollectionViewCell.registerCellNib(with: self.deviceTypeListView)
        self.deviceTypeListView.backgroundColor = UIColor.init(red: 255.0/255.0, green: 189.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        self.contentView.backgroundColor = .clear
        self.mainBGView.backgroundColor = UIColor.init(named: "devices_list_bg_color")
        self.mainBGView.layer.cornerRadius = 8
        self.mainBGView.layer.masksToBounds = false
        self.mainBGView.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.mainBGView.layer.shadowColor = UIColor.lightGray.cgColor
        self.mainBGView.layer.shadowOpacity = 0.7
        self.mainBGView.layer.shadowRadius = 4
        self.deviceTypeListView.layer.cornerRadius = 8
        self.deviceTypeListView.layer.masksToBounds = false
        self.prepareForReuse()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.groupButton.isSelected = false
    }
    
    
    // MARK: - Actions
    
    @IBAction private func groupButtonTapped(_ sender: Any) {
        
    }
}

// MARK: - UICollectionView view data source methods
extension DevicesTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.myDevices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = DeviceTypeCollectionViewCell.dequeueCell(from: self.deviceTypeListView, atIndexPath: indexPath)
        cell.configure(isShadow:false)
        
        let mydevice = self.myDevices[indexPath.row]
        if !mydevice.deviceName.isEmpty {
            cell.deviceName.text = mydevice.deviceName
        } else {
            cell.deviceName.text = mydevice.modelName
        }
        
        cell.deviceImage.image = UIImage(named: String(mydevice.type.rawValue) + "_black_icon")
        cellDict[mydevice.udid] = indexPath.row
        cell.contentView.backgroundColor = UIColor(red: 230/256.0, green: 230/256.0, blue: 230/256.0, alpha:1)
        
        return cell
    }
    
    
    func getDevicePower(udid: String) -> Bool {
        return false
    }
    
    func getGroupPower(udids: [String]) -> Bool {
        return false
    }
    
}

// MARK: - UICollectionView  delegate FlowLayout  methods

extension DevicesTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenwidth = UIApplication.shared.keyWindow!.frame.width
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = screenwidth - paddingSpace - 36
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let device = self.myDevices[indexPath.row]
        self.didTappedOnDevice?(device)
    }
}

// MARK: - NibInitializable
extension DevicesTableViewCell: NibInitializable {}

// MARK: - Reusable
extension DevicesTableViewCell: Reusable {}
