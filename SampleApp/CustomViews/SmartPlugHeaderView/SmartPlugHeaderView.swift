//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit
import MagicSDK

class SmartPlugHeaderView: UIView {
    
    @IBOutlet private var contentView: UIView!
    
    @IBOutlet weak var voltageValueLbl: UILabel!
    @IBOutlet weak var currentVlaue: UILabel!
    @IBOutlet weak var factorValueLbl: UILabel!
    @IBOutlet weak var powerValueLbl: UILabel!
    @IBOutlet weak var lblTDS: UILabel!
    @IBOutlet weak var offLbl: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var currentAndPfBGView: UIView!
    @IBOutlet weak var voltageAndPowerBGView: UIView!
    @IBOutlet weak var geysrVoltgLbl: UILabel!
    @IBOutlet weak var geysrPwrLbl: UILabel!
    @IBOutlet weak var geysrCrrntLbl: UILabel!
    @IBOutlet weak var geysrPFLbl: UILabel!
    
    @IBOutlet weak var powerFactorBGView: UIView!
    @IBOutlet weak var voltageCurrentBgView: UIView!
    
    @IBOutlet weak var voltageCutOffLabel: UILabel!
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    func initSubviews() {
        let nib = UINib(nibName: "SmartPlugHeaderView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }    
    
    func reloadInfoHeaderView(_ plugState: StateReport?, selectedDevice: IoTfyDevice?) {
        if let pow = plugState?.settings[FeatureControl.POWER.rawValue]{
            self.contentView.backgroundColor = pow as! Bool == false ?  UIColor(red: 111.0/255.0, green: 111.0/255.0, blue: 111.0/255.0, alpha: 1.0) :  UIColor.clear
            if pow as! Bool == false{
                self.offLbl.isHidden = false
            }
            else{
                self.offLbl.isHidden = true
            }
            self.powerFactorBGView.isHidden = pow as! Bool == false
            self.voltageCurrentBgView.isHidden = pow as! Bool == false
            self.deviceImage.isHidden = pow as! Bool == false
        }
        self.voltageValueLbl.text = "\(plugState?.data[FeatureMetric.VOLTAGE.rawValue] ?? 0) V"
        let current = plugState?.data[FeatureMetric.CURRENT.rawValue] as? Int
        self.currentVlaue.text = "\(String(format: "%0.1f",current  ?? 0)) A"
        if let mpfValue = plugState?.data[FeatureMetric.POWER_FACTOR.rawValue] as? Double{
            if mpfValue >= 1.00{
             self.factorValueLbl.text = "\(String(format: "%0.2f", 0.99))"
            }else {
             self.factorValueLbl.text = "\(String(format: "%0.2f", plugState?.data[FeatureMetric.POWER_FACTOR.rawValue] as? Double ?? 0))"
            }
        }
        let measuredPow = plugState?.data[FeatureMetric.POWER.rawValue] as? Double
        self.powerValueLbl.text = "\(String(format: "%0.1f", measuredPow ?? 0)) W"
       
//        let fault = plugState?.data[FeatureControl.]
//        if plugState?.data[FeatureMetric.] == 1 || plugState?.fault == 2 {
//            self.powerFactorBGView.isHidden = true
//            self.voltageCurrentBgView.isHidden = true
//            self.voltageCutOffLabel.isHidden = false
//            self.voltageCutOffLabel.text = plugState?.fault == 1 ? "Low Voltage Cut Off" : "High Voltage Cut Off"
//        }
        
        
    }
    
}
