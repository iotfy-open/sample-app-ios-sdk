//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit

class AddDeviceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var deviceTitleLabel: UILabel!
    @IBOutlet weak var mainBGView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor  = .clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func tapOnEdit(_ sender: UIButton) {
        
    }
    
    @IBAction func tapOnFavourite(_ sender: UIButton) {
        
    }
    
}
