//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork

class WiFiInfoServiceManager: NSObject {
    
    func getWiFiSsid() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    break
                }
            }
        }
        return ssid
    }
}
