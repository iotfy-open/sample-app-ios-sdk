//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit

protocol VoltageCutOffTableViewCellDelegate {
    func didTappedOnEditHighVoltage(_ highVoltage: String)
    func didTappedOnEditLowVoltage(_ lowVoltage: String)
}

class VoltageCutOffTableViewCell: UITableViewCell {
    
    @IBOutlet weak var highVoltageTextField: UITextField!
    @IBOutlet weak var lowVoltageTextField: UITextField!
    
    @IBOutlet weak var highVoltageBtn: UIButton!
    @IBOutlet weak var lowVoltageBtn: UIButton!
    
    var delegate:VoltageCutOffTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func tapOnEditLowVoltage(_ sender: Any) {
        delegate?.didTappedOnEditLowVoltage(self.lowVoltageTextField.text!)
    }
    
    @IBAction func tapOnEditHighVoltage(_ sender: Any) {
        delegate?.didTappedOnEditHighVoltage(self.highVoltageTextField.text!)
    }
    
}



