import UIKit
import MagicSDK
import IQKeyboardManagerSwift


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        MagicSDK.SDK.shared.initializeWith(app: UIApplication.shared, appId: "", secret: "")
        MagicSDK.SDK.shared.setSDKLogLevel(.error)
        IQKeyboardManager.shared.enable = true
    
        self.moveToLoginScreen()
        return true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConnectDeviceWiFiFromPhone"), object: nil, userInfo: nil)
    }
    
    func moveToLoginScreen() -> Void {
        let loginOptionsVC = AppStoryboard.PreLogin.viewController(viewControllerClass: LoginOptionsVC.self)
        let navigationController = UINavigationController()
        navigationController.viewControllers = [loginOptionsVC]
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }

    func moveToLoaderScreen() -> Void {
        let loaderVC = AppStoryboard.Loader.viewController(viewControllerClass: LoaderVC.self)
        let navigationController = UINavigationController()
        navigationController.viewControllers = [loaderVC]
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    
    func moveToHomeScreen() -> Void {
        let homeVC = AppStoryboard.Main.viewController(viewControllerClass: HomeVC.self)
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeVC]
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()

    }
    
    
}

