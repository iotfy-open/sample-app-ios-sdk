//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import Foundation
import UIKit

extension UIColor {
    static let defaultColor = UIColor(red: 191/255.0, green: 196/255.0, blue: 212/255.0, alpha: 1.0)
    static let errorColor =  UIColor(red: 255/255.0, green: 0.0/255.0, blue: 75/255.0, alpha: 1.0)
    static let defaultFontColor =  UIColor(red: 128/255.0, green: 137.0/255.0, blue: 168.0/255.0, alpha: 1.0)
    static let greenManageStateColor = UIColor(red: 46.0/255.0, green: 126.0/255.0, blue: 47.0/255.0, alpha: 1.0)
    static let redManageStateColor = UIColor(red: 238/255.0, green: 42.0/255.0, blue: 42.0/255.0, alpha: 1.0)
    static let offManageStateFontColor = UIColor(red: 106.0/255.0, green: 106.0/255.0, blue: 106.0/255.0, alpha: 1.0)
    
    func rgb() -> (red:Int, green:Int, blue:Int, alpha:Int)? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
    
    class func quickActionsBlueColor() -> UIColor {
        return UIColor(named: "quick_actions_accent_color")!
    }
    
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt32 = 0
        
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        let length = hexSanitized.count
        
        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }
        
        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0
            
        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0
            
        } else {
            return nil
        }
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
    
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to String
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
    func getColorDecimalValue() -> Int {
        
        let hexValue = String(format:"%02X", Int(self.rgb()!.red * 255)) + String(format:"%02X", Int(self.rgb()!.green * 255)) + String(format:"%02X", Int(self.rgb()!.blue * 255))
        return Int(hexValue, radix: 16)!
    }
    
    func getRGBValueFromDecimal(_ colorDecimal: Int) -> UIColor {
        let colorHexString = String(colorDecimal, radix: 16)
        return  UIColor.init(hexString: colorHexString)
    }
    
    convenience init(temperature : CGFloat)
    {
        let red, green, blue : CGFloat
        
        let percentKelvin = temperature / 100;
        
        red = clamp(value: percentKelvin <= 66 ? 255 : (329.698727446 * pow(percentKelvin - 60, -0.1332047592)));
        
        green = clamp(value: percentKelvin <= 66 ? (99.4708025861 * log(percentKelvin) - 161.1195681661) : 288.1221695283 * pow(percentKelvin, -0.0755148492));
        
        blue = clamp(value: percentKelvin >= 66 ? 255 : (percentKelvin <= 19 ? 0 : 138.5177312231 * log(percentKelvin - 10) - 305.0447927307));
        
        self.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }
    
}

private func clamp (value : CGFloat) -> CGFloat {
    return value > 255 ? 255 : (value < 0 ? 0 : value);
}

enum StatusBarColor {
    case statusBarAppThemeColor
    case statusBarTypePinkColor
    case statusBarTypeGrayColor
    case statusBarTypeDarkGrayColor
    case statusBarTypeWhiteColor
    case statusBarTypeNoColor
    case statusBarTypeRedColor
    case statusBarNavigationRedColor
}

extension StatusBarColor {
    var value: UIColor {
        get {
            switch self {
                case .statusBarAppThemeColor:
                    return UIColor(named: "appButtonBGColor")!
                case .statusBarTypePinkColor:
                    return UIColor(red: 255.0/255.0, green: 117.0/255.0, blue: 174.0/255.0, alpha: 1.0)
                case .statusBarTypeGrayColor:
                    return UIColor(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 1.0)
                case .statusBarTypeDarkGrayColor:
                    return UIColor(red: 111.0/255.0, green: 111.0/255.0, blue: 111.0/255.0, alpha: 1.0)
                case .statusBarTypeRedColor:
                    return UIColor(red: 249.0/255.0, green: 51.0/255.0, blue: 49.0/255.0, alpha: 1.0)
                case .statusBarTypeWhiteColor:
                    return UIColor.white
                case .statusBarNavigationRedColor:
                    return UIColor.init(red: 244.0/255.0, green: 44.0/255.0, blue: 54.0/255.0, alpha: 1.0)
                case .statusBarTypeNoColor:
                    return UIColor.clear
            }
        }
    }
    
    
}

extension UIColor {
    convenience init(hexString:String) {
        let scanner            = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
}
