//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//


import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate

enum DeviceType: Int, Codable {
    case deviceTypePlug = 6
    case deviceTypeNone = 1001
    var code: Int {
        return self.rawValue
    }
}

let plugKeysArray = ["pow", "input", "timer"]
let plugTimer  = ["No Timer","1HR","2HR","4HR","6HR"]

enum PlugDeviceTimes: Int {
    case plug1HRTime = 1
    case plug2HRTime = 2
    case plug4HRTime = 4
    case plug6HRTime = 6
    
    var code: Int {
        return self.rawValue
    }
}

