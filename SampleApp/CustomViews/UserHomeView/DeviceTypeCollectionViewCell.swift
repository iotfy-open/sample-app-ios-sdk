//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit

class DeviceTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var roomName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.autoresizingMask.insert(.flexibleHeight)
        self.contentView.autoresizingMask.insert(.flexibleWidth)
        self.contentView.backgroundColor = .clear
    }
    
    func configure(isShadow: Bool = true) {
        
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 4;
        self.contentView.layer.cornerRadius = 6.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        
        if isShadow == true {
            self.layer.shadowColor = UIColor.lightGray.cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            self.layer.shadowRadius = 2.0
            self.layer.shadowOpacity = 1.0
            self.layer.masksToBounds = false
            self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds,
                                                 cornerRadius: self.contentView.layer.cornerRadius).cgPath
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    
}


// MARK: - NibInitializable
extension DeviceTypeCollectionViewCell: NibInitializable {}

// MARK: - Reusable
extension DeviceTypeCollectionViewCell: Reusable {}
