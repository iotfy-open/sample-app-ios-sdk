//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit

protocol DoubleButtonTableViewCellDelegate {
    func didTappedOnFirstButton(_ sender: UIButton)
    func didTappedOnSecondButton(_ sender: UIButton)
}

class DoubleButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    
    var delegate:DoubleButtonTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func tapOnFirstButton(_ sender: UIButton) {
        delegate?.didTappedOnFirstButton(sender)
    }
    @IBAction func tapOnSecondButton(_ sender: UIButton) {
        delegate?.didTappedOnSecondButton(sender)
    }
    
}
