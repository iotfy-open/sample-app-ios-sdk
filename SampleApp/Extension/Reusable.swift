//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import Foundation

import UIKit

public protocol Reusable {
    static var reuseIdentifier: String { get }
}

public extension Reusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

public extension Reusable where Self: UITableViewCell {
    static func register(with tableView: UITableView) {
        tableView.register(Self.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    static func dequeueCell(from tableView: UITableView, atIndexPath indexPath: IndexPath) -> Self {
        return tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! Self
    }
}

public extension Reusable where Self: UITableViewHeaderFooterView {
    static func register(with tableView: UITableView) {
        tableView.register(Self.self, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
    }
    
    static func dequeueHeaderFooter(from tableView: UITableView) -> Self {
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseIdentifier) as! Self
    }
}

public extension Reusable where Self: UICollectionViewCell {
    static func register(with collectionView: UICollectionView) {
        collectionView.register(Self.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    static func dequeueCell(from collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> Self {
        return collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! Self
    }
}

public extension Reusable where Self: UICollectionReusableView {
    static func registerSectionHeader(with collectionView: UICollectionView) {
        collectionView.register(Self.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: reuseIdentifier)
    }
    
    static func dequeueHeader(from collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> Self {
        return collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: reuseIdentifier, for: indexPath) as! Self
    }
    
    static func registerSectionFooter(with collectionView: UICollectionView) {
        collectionView.register(Self.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: reuseIdentifier)
    }
    
    static func dequeueFooter(from collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> Self {
        return collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: reuseIdentifier, for: indexPath) as! Self
    }
}
