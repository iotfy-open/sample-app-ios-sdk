//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import Foundation


import UIKit

enum AppStoryboard : String {
    
    case Main = "Main"
    case PreLogin = "PreLogin"
    case Loader = "Loader"
    case Device = "Device"
    
    
    /*
     * To Get Story Board
     * ie.let mainStoryboard = AppStoryboard.Main.instance
     */
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    
    /*
     * To Initialize View Controller
     * Function, Line & File Just to find out Error
     * ie. let exampleScene = AppStoryboard.Main.viewController(viewControllerClass: ExampleVC.self)
     */
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function, line : Int = #line, file : String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        
        return scene
    }
    
    
    /*
     * To Get Initial View Controller
     * ie.let exampleScene = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ExampleVC.storyboardID)
     */
    func initialViewController() -> UIViewController? {
        
        return instance.instantiateInitialViewController()
    }
    
}


extension UIViewController {
    
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID : String {
        
        return "\(self)"
    }
    
    /*
     *let exampleScene = ExampleVC.instantiate(fromAppStoryboard: .Main)
     */
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
