//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit
import MagicSDK

class ConnectDeviceVC: UIViewController {
    
    @IBOutlet weak var passwordTextField: AppTextField!
    @IBOutlet weak var wifiTextField: AppTextField!
    @IBOutlet weak var responseValue: UILabel!
    @IBOutlet weak var showPasswordBtn:UIButton!
    @IBOutlet weak var directConnect:UIButton!
    @IBOutlet weak var directConnectLabel: UILabel!
    @IBOutlet weak var backButton:UIButton!
    @IBOutlet weak var wifiSwitchButton: UIButton!
    @IBOutlet weak var typeConnectTitle: UILabel!
    @IBOutlet weak var typeConnectImage: UIImageView!
    @IBOutlet weak var typeConnectHint: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var wifiInfoBgView: UIView!
    
    
    var deviceUDIDValue: String =  ""
    var deviceSSIDValue:String = ""
    var devicePasswordValue:String = ""
    var isFromUpdateWifi =  false
    var isFromHome = false
    var isFromQuickConnect = false
    var isFromSettings = false
    
    // MARK: - Private
    private var selectedDevice: IoTfyDevice?
    
    
    // MARK: - Life Cycle
    class func instantiate(selectedDevice: IoTfyDevice?, isFromHome: Bool = false, isFromQuickConnect: Bool = false, isFromUpdateWifi: Bool = false, isFromSettngs: Bool = false) -> ConnectDeviceVC {
        
        let controller = AppStoryboard.Device.viewController(viewControllerClass: ConnectDeviceVC.self)
        controller.isFromHome = isFromHome
        controller.isFromQuickConnect = isFromQuickConnect
        controller.isFromUpdateWifi = isFromUpdateWifi
        controller.isFromSettings = isFromSettngs
        
        guard let selectedDevice = selectedDevice else {
            controller.deviceUDIDValue = ""
            controller.deviceSSIDValue = ""
            controller.devicePasswordValue = ""
            return controller
        }
        controller.selectedDevice = selectedDevice
        controller.deviceUDIDValue = selectedDevice.udid
        controller.deviceSSIDValue = selectedDevice.userWifiSsid
        
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.batteryLevelChanged),
            name: NSNotification.Name(rawValue: "ConnectDeviceWiFiFromPhone"),
            object: nil)
        
        if isFromQuickConnect{
            self.directConnect.isHidden = true;
            self.directConnectLabel.isHidden = true;
            self.backButton.isHidden = false
            self.typeConnectTitle.text = "Quick Connect"
            self.typeConnectImage.image = UIImage.init(named: "global_connect_gray_icon")
            self.typeConnectHint.text = "Select a 2.4GHz Wi-Fi Network and enter password"
        }
        let wifiInfoServiceManager = WiFiInfoServiceManager()
        if let wifiValue = wifiInfoServiceManager.getWiFiSsid() {
            self.wifiTextField.isUserInteractionEnabled = false
            self.wifiTextField.text = wifiValue
            
            //show password if already in UserDefalts.
            self.passwordTextField.text = "" // this will clear previous value.
            
        }
    }
    @IBAction func tapOnConnectDevice(_ sender: Any) {
        
        if checkEmptyWiFiPasswordFields() {
            return
        }
        
        if isFromQuickConnect { //Quick
            self.showProcessingView()
        }
        
        else{
            if deviceSSIDValue != "" && devicePasswordValue != "" && self.wifiTextField.text?.isFieldEmpty == false && self.passwordTextField.text?.isFieldEmpty == false { //WiFi
                self.showProcessingView()
            }
        }
    }
    
    @IBAction func tapOnBack(_ sender: Any) {
        if isFromQuickConnect {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.popViewController(animated: true)
        }
        
        else if !isFromUpdateWifi {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func tapOnSwitchNetwork(_ sender: UIButton) {
        
        let alertController = UIAlertController.init(title: "Switch Wi-Fi", message: "Do you want to swicth your Wi-Fi connection?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Switch Wi-Fi", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
        
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func tapOnShowHidePassword(_ sender: UIButton) {
        if self.passwordTextField.isSecureTextEntry {
            let currentText: String = self.passwordTextField.text!
            self.passwordTextField.isSecureTextEntry = false
            self.showPasswordBtn.setImage(UIImage.init(named: "show_password_icon"), for: UIControl.State.normal)
            self.passwordTextField.text = "";
            self.passwordTextField.text = currentText
        }
        
        else{
            self.passwordTextField.isSecureTextEntry = true
            self.showPasswordBtn.setImage(UIImage.init(named: "hide_password_icon"), for: UIControl.State.normal)
            let currentText: String = self.passwordTextField.text!
            self.passwordTextField.text = "";
            self.passwordTextField.text = currentText
        }
    }
    
    func showProcessingView(isFromQuickConnectView:Bool = false)  {
        
        var connectProcessingVC: ConnectProcessingVC!
        
        if isFromQuickConnect {
            connectProcessingVC = ConnectProcessingVC.instantiate()
            connectProcessingVC.isFromQuickConnect = true
            connectProcessingVC.userSSIDName = self.wifiTextField.text ?? ""
            connectProcessingVC.userWiFiPassword = self.passwordTextField.text ?? ""
        } else {
            connectProcessingVC = ConnectProcessingVC.instantiate()
            connectProcessingVC.userSSIDName = self.wifiTextField.text ?? ""
            connectProcessingVC.userWiFiPassword = self.passwordTextField.text ?? ""
            connectProcessingVC.deviceUDID = self.deviceUDIDValue == "" ? self.selectedDevice!.udid : self.deviceUDIDValue
            connectProcessingVC.ssidValue = self.deviceSSIDValue == "" ? self.selectedDevice!.userWifiSsid : self.deviceSSIDValue
        }
        
        connectProcessingVC.delegate = self
        connectProcessingVC.modalPresentationStyle = .fullScreen
        
        self.present(connectProcessingVC, animated: true, completion: nil)
        
    }
    
    func checkEmptyWiFiPasswordFields() -> Bool {
        if self.wifiTextField.text?.isFieldEmpty == true {
            self.wifiTextField.showTextError("Please connect from WiFi")
        }
        if self.passwordTextField.text?.isFieldEmpty == true {
            self.passwordTextField.showTextError("Please enter WiFi password")
        }
        if passwordTextField.text?.isFieldEmpty == true || wifiTextField.text?.isFieldEmpty == true {
            return true
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == wifiTextField {
            wifiTextField.hideTextError()
        } else {
            passwordTextField.hideTextError()
        }
    }
    
    @objc private func batteryLevelChanged(notification: NSNotification){
        let wifiInfoServiceManager = WiFiInfoServiceManager()
        if let wifiValue = wifiInfoServiceManager.getWiFiSsid() {
            self.wifiTextField.text = wifiValue
            self.passwordTextField.text = ""
        }
    }
}

extension ConnectDeviceVC:ConnectProcessingDelegate {
    func didFinishTaskStats() {
        print("didFinishTaskStats")
    }
    
    func didFailedTaskStatus() {
        print("didFailedTaskStatus")
    }
}
