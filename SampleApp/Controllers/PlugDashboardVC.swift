//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit
import MagicSDK

class PlugDashboardVC: UIViewController {
    
    let smartPlugheaderValue = ["","Voltage","Timer"]
    
    @IBOutlet weak var smartPlugDashboardTableView: UITableView!
    let OnImageName = "on_button_icon"
    private var selectedDeviceValue: IoTfyDevice?
    var isBackButtonPressed = false

    var deviceState: StateReport?
    
    class func instantiate(selectedDevice: IoTfyDevice?) -> PlugDashboardVC {
        let controller = AppStoryboard.Device.viewController(viewControllerClass: PlugDashboardVC.self)
        controller.selectedDeviceValue = selectedDevice
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCell()
        
    }
    private func registerTableViewCell() {
        self.smartPlugDashboardTableView.register(UINib(nibName: "DeviceInfoHeaderCell", bundle: nil), forCellReuseIdentifier: "DeviceInfoHeaderCell")
        self.smartPlugDashboardTableView.register(UINib(nibName: "DoubleButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "DoubleButtonTableViewCell")
        self.smartPlugDashboardTableView.register(UINib(nibName: "HeaderTableViewCell", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "HeaderTableViewCell")
        self.smartPlugDashboardTableView.register(UINib(nibName: "VoltageCutOffTableViewCell", bundle: nil), forCellReuseIdentifier: "VoltageCutOffTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MagicSDK.SDK.shared.getThingService()?.registerStateChangeListener(udid: selectedDeviceValue!.udid, thingChangeStateCb: self)
        self.smartPlugDashboardTableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
    }
    
    func enableOrDisableButton( isButtonEnabled:Bool, sender:UIButton) {
        sender.isUserInteractionEnabled =  isButtonEnabled
        if !isButtonEnabled {
            sender.alpha = 0.5
        }else {
            sender.alpha = 1.0
        }
    }
    
}

extension PlugDashboardVC:UITableViewDataSource{
    // MARK: - UITable view data source method
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return plugKeysArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let valueAtIndex = plugKeysArray[section]
        if valueAtIndex == "timer" {
            return 2
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let valueAtIndex = plugKeysArray[indexPath.section]
        if valueAtIndex == "pow" {
            let cell = self.smartPlugDashboardTableView.dequeueReusableCell(withIdentifier: "DeviceInfoHeaderCell") as! DeviceInfoHeaderCell
            cell.setUpForPlug(self.deviceState, selectedDevice: selectedDeviceValue)
            cell.delegate = self
            
            return cell
        }
        
        if valueAtIndex == "input" {
            let cell = self.smartPlugDashboardTableView.dequeueReusableCell(withIdentifier: "VoltageCutOffTableViewCell") as! VoltageCutOffTableViewCell
            cell.delegate = self
            cell.highVoltageTextField.text = "\(self.deviceState?.settings[FeatureControl.HIGH_VOLTAGE_CUTOFF.rawValue] ?? 270)"
            cell.lowVoltageTextField.text = "\(self.deviceState?.settings[FeatureControl.LOW_VOLTAGE_CUTOFF.rawValue] ?? 170)"
            if let pow = self.deviceState?.settings[FeatureControl.POWER.rawValue]{
                self.enableOrDisableButton(isButtonEnabled: pow as! Bool, sender: cell.highVoltageBtn)
                self.enableOrDisableButton(isButtonEnabled: pow as! Bool, sender: cell.lowVoltageBtn)
            }
           
            
            return cell
        }
        
        
        
        let cell = self.smartPlugDashboardTableView.dequeueReusableCell(withIdentifier: "DoubleButtonTableViewCell") as! DoubleButtonTableViewCell
        cell.delegate = self
        
        var hourValue = 0
        if let timeValue = self.deviceState?.settings[FeatureControl.OFF_TIMER.rawValue] {
            hourValue = timeValue as! Int/3600
        }
        
        
        cell.firstButton.setBackgroundImage(UIImage.init(named:"off_button_icon"), for: .normal)
        cell.secondButton.setBackgroundImage(UIImage.init(named:"off_button_icon"), for: .normal)
        
        if indexPath.row == 0{
            cell.firstButton.setTitle(plugTimer[1], for: .normal)
            cell.secondButton.setTitle(plugTimer[2], for: .normal)
            if hourValue == 1 {
                cell.firstButton.setBackgroundImage(UIImage.init(named:OnImageName), for: .normal)
            } else if hourValue == 2 {
                cell.secondButton.setBackgroundImage(UIImage.init(named:OnImageName), for: .normal)
            }
        } else {
            cell.firstButton.setTitle(plugTimer[3], for: .normal)
            cell.secondButton.setTitle(plugTimer[4], for: .normal)
            if hourValue == 4 {
                cell.firstButton.setBackgroundImage(UIImage.init(named:OnImageName), for: .normal)
            }else if hourValue == 6 {
                cell.secondButton.setBackgroundImage(UIImage.init(named:OnImageName), for: .normal)
            }
        }
        if let pow = self.deviceState?.settings[FeatureControl.POWER.rawValue]{
            self.enableOrDisableButton(isButtonEnabled: pow as! Bool, sender: cell.firstButton)
            self.enableOrDisableButton(isButtonEnabled: pow as! Bool, sender: cell.secondButton)
        }
        cell.firstButton.tag = indexPath.section * 11 + indexPath.row
        cell.secondButton.tag = indexPath.section * 13 + indexPath.row
        
        return cell
    }
}
extension PlugDashboardVC:UITableViewDelegate{
    
    // MARK: - UITable view delegate methods
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 0{
            return 0.001
        }
        return 30
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView =  self.smartPlugDashboardTableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
        headerView.headerLabel.textColor = UIColor.black
        headerView.headerLabel.text = smartPlugheaderValue[section]
        headerView.backgroundColor = .white
        headerView.contentView.backgroundColor = .clear
        headerView.contentView.backgroundColor = .white
        headerView.backgroundColor = .white
        return headerView
    }
}

extension PlugDashboardVC:DoubleButtonTableViewCellDelegate {
    
    func didTappedOnFirstButton(_ sender: UIButton) {
        let tagValue = sender.tag/11
        switch (tagValue) {
            case 1:
                break;
            case 2:
                if sender.tag % 11 == 1{
                    self.managePlugTime(PlugDeviceTimes.plug4HRTime)
                }else{
                    self.managePlugTime(PlugDeviceTimes.plug1HRTime)
                }
                break;
            default:
                break;
        }
    }
    
    func didTappedOnSecondButton(_ sender: UIButton) {
        
        let tagValue = sender.tag/13
        switch (tagValue) {
            case 1:
                break;
            case 2:
                if sender.tag % 13 == 1{
                    self.managePlugTime(PlugDeviceTimes.plug6HRTime)
                }else{
                    self.managePlugTime(PlugDeviceTimes.plug2HRTime)
                }
                break;
            default:
                break;
        }
    }
    
    func managePlugTime(_ deviceTime: PlugDeviceTimes) {
        
        var hourValue = 0
        if let timeValue = self.deviceState?.settings[FeatureControl.OFF_TIMER.rawValue] {
            hourValue = timeValue as! Int/3600
        }
        if hourValue == deviceTime.rawValue {
            MagicSDK.SDK.shared.getThingService()?.updateState(udid: selectedDeviceValue!.udid, command: [FeatureControl.OFF_TIMER.rawValue:0])

        } else {
            MagicSDK.SDK.shared.getThingService()?.updateState(udid: selectedDeviceValue!.udid, command: [FeatureControl.OFF_TIMER.rawValue:deviceTime.rawValue * 3600])

        }
    }
}

extension PlugDashboardVC:DeviceInfoHeaderCellDelegate {
    func didTappedOnPowerButton(_ sender: UIButton) {
        let currentPowerState = self.deviceState?.settings[FeatureControl.POWER.rawValue]
        if (currentPowerState as? Int == 1) {
            MagicSDK.SDK.shared.getThingService()?.updateState(udid: selectedDeviceValue!.udid, command: [FeatureControl.POWER.rawValue: 0])
        } else {
            MagicSDK.SDK.shared.getThingService()?.updateState(udid: selectedDeviceValue!.udid, command: [FeatureControl.POWER.rawValue: 1])
        }
    }
    
    func didTappedOnBackButton(_ sender: UIButton) {
        
        self.isBackButtonPressed = true
        appDelegate.moveToHomeScreen()
    }
    
    func didSwitchedInNetworkConnection(_ sender: UIButton) {
        print("Is Switched On \(sender.isSelected)")
        
    }
}


extension PlugDashboardVC: VoltageCutOffTableViewCellDelegate,UITextFieldDelegate {
    
    func didTappedOnEditHighVoltage(_ highTemp: String) {
        
    }
    
    func didTappedOnEditLowVoltage(_ lowTemp: String) {
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 3
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

extension PlugDashboardVC: ThingStateChangeCb  {
    func onStateReported(udid: String, state: StateReport) {
        print("State = \(state)")
        self.deviceState = state
    }
    
    func onDeviceConnectionStateChanged(udid: String, isOnline: Bool) {
        print("isOnline \(isOnline)")
    }
    
   
}

