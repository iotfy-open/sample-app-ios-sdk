//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import Foundation
import UIKit

extension UITableView {
    
    /// Remove cell separators for empty cells.
    /// Current Implementation is setting the tableFooterView.
    func preventEmptyCellSeparators() {
        self.tableFooterView = UIView(frame: CGRect.zero)
    }
}
