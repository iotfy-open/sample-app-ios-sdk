//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit
import MagicSDK

let gradientOne = UIColor(red: 255.0/255.0, green: 117.0/255.0, blue: 174.0/255.0, alpha: 1.0).cgColor
let gradientTwo = UIColor(red: 251/255.0, green: 97.0/255.0, blue: 98.0/255.0, alpha: 1.0).cgColor

protocol DeviceInfoHeaderCellDelegate {
    func didTappedOnPowerButton(_ sender: UIButton)
    func didTappedOnBackButton(_ sender: UIButton)
    func didSwitchedInNetworkConnection(_ sender: UIButton)
}

class DeviceInfoHeaderCell: UITableViewCell {
    
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var modeTypeImageView: UIImageView!
    @IBOutlet weak var modeTypeValueLabel: UILabel!
    @IBOutlet weak var deviceOnOffButton: UIButton!
    @IBOutlet weak var offLabel: UILabel!
    @IBOutlet weak var modeTypeBGView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var animatedView: UIView!
    @IBOutlet weak var circularGradiantView: UIView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var backButton:UIButton!
    @IBOutlet weak var remainingTimeLbl:UILabel!
    @IBOutlet weak var remainingTimeBGView: UIView!
    @IBOutlet weak var networkConnectSwitch: UIButton!
    @IBOutlet weak var tempratureImage: UIImageView!
    @IBOutlet weak var humidityImage: UIImageView!
    @IBOutlet weak var localInternetImageView: UIImageView!
    @IBOutlet weak var globalInternetImageView: UIImageView!
    @IBOutlet weak var roomNameLabel: UILabel!
    
    @IBOutlet weak var tempStackView: UIStackView!
    @IBOutlet weak var customBGView: UIView!
    var delegate:DeviceInfoHeaderCellDelegate?
    var gradientLayer: CAGradientLayer!
    var endLocations : [NSNumber] = [0.4,1.0,1.2]
    var startLocations: [NSNumber] = [-1.0,-0.1,0.0]
    var movingAnimationDuration : CFTimeInterval = 7.0
    var delayBetweenAnimationLoops:CFTimeInterval = 0.01
    var timer: Timer?
    var smartPlugHeaderView: SmartPlugHeaderView = SmartPlugHeaderView()
    
    @IBOutlet weak var wifiDirectConnectBGView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.networkConnectSwitch.layer.cornerRadius = 16
        
    }
    
    func startAnimating(){
        
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = self.endLocations
        animation.toValue = self.startLocations
        animation.duration = self.movingAnimationDuration
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        
        let animationGroup = CAAnimationGroup()
        animationGroup.duration = self.movingAnimationDuration + self.delayBetweenAnimationLoops
        animationGroup.animations = [animation]
        animationGroup.repeatCount = .infinity
        self.gradientLayer.add(animationGroup, forKey: animation.keyPath)
        
    }
    
    func stopAnimating() {
        self.gradientLayer.removeAllAnimations()
        self.gradientLayer.removeFromSuperlayer()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func secondsToHoursMinutesSeconds(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        return String(format: "%01i Hour(s) and %02i Minute(s) left", hours, minutes)
    }
    
    func setUpForPlug( _ plugState: StateReport? ,selectedDevice: IoTfyDevice?) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.animatedView.bounds
        gradientLayer.colors = [
            gradientOne,
            gradientTwo,
            gradientOne,
            gradientTwo
        ]
        gradientLayer.locations = self.startLocations
        self.animatedView.layer.addSublayer(gradientLayer)
        self.gradientLayer = gradientLayer
        gradientLayer.frame = CGRect(x:0, y:0, width:414, height: self.animatedView.frame.size.height)
        self.startAnimating()
        //Show On Off
        let origImage = UIImage(named: "dashboard_back_icon")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        self.backButton.setImage(tintedImage, for: .normal)
        self.backButton.tintColor = .white
        
        self.bgView.backgroundColor = UIColor.clear
        if let pow = plugState?.settings[FeatureControl.POWER.rawValue]{
            if let image  = UIImage(named: pow as! Bool == false ? "device_off_icon" : "device_on_icon") {
                self.deviceOnOffButton.setImage(image, for: .normal)
            }
        }
        
        self.roomNameLabel.textColor = .white
        if ((plugState?.settings[FeatureControl.POWER.rawValue]) != nil) { //Device is On
            self.roomNameLabel.textColor = .white
        }
        
        self.animatedView.isHidden = false
        self.smartPlugHeaderView.removeFromSuperview()
        
        smartPlugHeaderView = SmartPlugHeaderView.init()
        self.smartPlugHeaderView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height: self.animatedView.frame.size.height)
        self.smartPlugHeaderView.backgroundColor = .clear
        self.smartPlugHeaderView.reloadInfoHeaderView(plugState, selectedDevice: selectedDevice)
        self.customBGView.addSubview(smartPlugHeaderView)
        
        
    }
    
    
    @IBAction func tapOnPowerButton(_ sender: UIButton) {
        delegate?.didTappedOnPowerButton(sender)
    }
    @IBAction func tapOnBackButton(_ sender: UIButton) {
        delegate?.didTappedOnBackButton(sender)
    }
    
}


