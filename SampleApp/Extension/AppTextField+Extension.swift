//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import Foundation
import UIKit

extension  AppTextField {
    
    func showError(_ errorMessage : String) -> Void {
        
        if errorMessage != "" {
            
            self.showInfo(errorMessage)
            self.placeholderColor = UIColor.errorColor
            self.lineColor = UIColor.errorColor
            
        }else {
            
            self.showInfo(errorMessage)
            self.placeholderColor = UIColor.defaultFontColor
            self.lineColor = UIColor.defaultColor
        }
    }
    
    
    func showTextError(_ errorMessage : String?){
        guard let errorMsg = errorMessage else {
            return
        }
        self.showInfo(errorMsg)
        self.placeholderColor = UIColor.errorColor
        self.lineColor = UIColor.errorColor
    }
    
    func hideTextError(){
        self.showInfo("")
        self.placeholderColor = UIColor.defaultFontColor
        self.lineColor = UIColor.defaultColor
    }
    
}
