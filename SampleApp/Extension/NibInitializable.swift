//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import Foundation
import UIKit

public protocol NibInitializable {
    static func nibName() -> String
    static var nib: UINib { get }
}

public extension NibInitializable {
    static func nibName() -> String {
        let name = String(describing: self)
        assert(!name.contains("."), "\(name) must not contain \".\" ")
        
        // remove any generics: <T>, etc. froms string.
        return name.replacingOccurrences(of: "<[^>]*?>", with: "", options: [.regularExpression, .caseInsensitive], range: name.range(of: name))
    }
    
    static var nib: UINib {
        return UINib(nibName: nibName(), bundle: nil)
    }
}

// MARK: - UIView
public extension NibInitializable where Self: UIView {
    static func viewFromNib() -> Self {
        return Bundle.main.loadNibNamed(self.nibName(), owner: nil, options: nil)?.first as! Self
    }
}

// MARK: - UIViewController
public extension NibInitializable where Self: UIViewController {
    static func viewControllerFromNib() -> Self {
        return self.init(nibName: self.nibName(), bundle: nil)
    }
}

// MARK: - UITableViewCell
public extension NibInitializable where Self: UITableViewCell {
    static func registerCellNib(with tableView: UITableView) {
        tableView.register(nib, forCellReuseIdentifier: nibName())
    }
}

// MARK: - UITableViewHeaderFooterView
public extension NibInitializable where Self: UITableViewHeaderFooterView {
    static func registerHeaderFooterNib(with tableView: UITableView) {
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: nibName())
    }
}

// MARK: - UICollectionViewCell
public extension NibInitializable where Self: UICollectionViewCell {
    static func registerCellNib(with collectionView: UICollectionView) {
        collectionView.register(nib, forCellWithReuseIdentifier: nibName())
    }
}

// MARK: - UICollectionReusableView
public extension NibInitializable where Self: UICollectionReusableView {
    static func registerSectionHeaderNib(with collectionView: UICollectionView) {
        collectionView.register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: nibName())
    }
    
    static func registerSectionFooterNib(with collectionView: UICollectionView) {
        collectionView.register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: nibName())
    }
}
