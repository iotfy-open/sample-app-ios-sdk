//  SampleApp
//
//  Created by IoTfy on 01/11/21.
//

import UIKit

class LoaderVC: UIViewController {
    
    @IBOutlet weak var versionNumber: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let versionText = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let buildText = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        if (versionText != nil && buildText != nil) {
            self.versionNumber.text = "\(versionText ?? "") (\(buildText ?? ""))"
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            appDelegate.moveToHomeScreen()
        }
    }
}
